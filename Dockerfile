FROM adoptopenjdk/openjdk11
WORKDIR /opt
ENV PORT 8084
EXPOSE 8084
COPY kazi-rest/target/*-exec.jar /opt/kazi-mail.jar
ENTRYPOINT ["java", "-jar", "kazi-mail.jar"]