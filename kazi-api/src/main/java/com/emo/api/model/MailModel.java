package com.emo.api.model;

import org.springframework.core.io.InputStreamSource;

public class MailModel {

    private String to;
    private String subject;
    private String content;
    private String attachmentFilename;
    private InputStreamSource inputStreamSource;

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public String getAttachmentFilename() {
        return attachmentFilename;
    }

    public InputStreamSource getInputStreamSource() {
        return inputStreamSource;
    }
}
