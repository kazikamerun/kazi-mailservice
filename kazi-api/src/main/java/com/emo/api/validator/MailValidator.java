package com.emo.api.validator;

import com.emo.api.model.MailModel;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Service
public class MailValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return MailModel.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final MailModel mailModel = (MailModel) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "to", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "NotEmpty");

        if (!mailModel.getTo().matches("^(.+)@(.+)$")) {
            errors.rejectValue("to", "Email.no.conform");
        }
    }
}