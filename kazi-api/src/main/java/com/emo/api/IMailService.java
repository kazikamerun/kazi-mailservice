package com.emo.api;

import com.emo.api.model.MailModel;

public interface IMailService {

    void sendEmail(final MailModel mailModel);
}
