package com.emo.rest.bootstrap;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Setter
@Getter
@Configuration
@ConfigurationProperties("oauth.server.security")
public class OAuth2Configuration {

    @Setter
    @Getter
    public static class Password {
        private String id;
        private String secret;
    }

    private Password password;
    private String tokenEndpoint;
}
