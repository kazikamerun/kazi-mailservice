package com.emo.rest.resource;


import com.emo.rest.bootstrap.OAuth2Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

    @Autowired
    private OAuth2Configuration oauth2Configuration;

    @Override
    public void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/xyz/**")
                .authenticated()
                .and()
                .authorizeRequests()
                .antMatchers("/")
                .permitAll();
    }

    @Primary
    @Bean
    public RemoteTokenServices tokenServices() {
        RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl(oauth2Configuration.getTokenEndpoint());
        tokenService.setClientId(oauth2Configuration.getPassword().getId());
        tokenService.setClientSecret(oauth2Configuration.getPassword().getSecret());
        return tokenService;
    }
}
