package com.emo.rest.mail;

import com.emo.api.IMailService;
import com.emo.api.model.MailModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailService implements IMailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(final MailModel model) {
        final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            final MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");

            messageHelper.setTo(model.getTo());
            messageHelper.setSubject(model.getSubject());
            messageHelper.setValidateAddresses(true);
            messageHelper.setText(model.getContent(), true);
            if (model.getAttachmentFilename() != null && model.getInputStreamSource() != null) {
                messageHelper.addAttachment(model.getAttachmentFilename(), model.getInputStreamSource());
            }

            /*final FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
            messageHelper.addAttachment("Invoice", file);*/
            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {

        }
    }
}
