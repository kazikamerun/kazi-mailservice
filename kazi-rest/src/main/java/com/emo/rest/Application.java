package com.emo.rest;

import com.emo.api.validator.MailValidator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

    @Bean
    public MailValidator mailValidator() {
        return new MailValidator();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
