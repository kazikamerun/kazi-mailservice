package com.emo.rest.controller;

import com.emo.api.model.MailModel;
import com.emo.api.validator.MailValidator;
import com.emo.rest.mail.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/mail", produces = "application/json")
public class SendController {

    @Autowired
    private MailService mailService;
    @Autowired
    private MailValidator mailValidator;

    @PostMapping(value = "/sent", consumes = "application/json")
    public ResponseEntity<String> sendSimpleMail(@RequestBody MailModel mailModel, final BindingResult bindingResult) {

        mailValidator.validate(mailModel, bindingResult);
        mailService.sendEmail(mailModel);
        return new ResponseEntity<>("Mail sent", HttpStatus.OK);
    }
}
